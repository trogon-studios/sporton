﻿//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

using System;
using System.Diagnostics;
using System.Threading;
using Windows.ApplicationModel.Background;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Storage;
using Windows.System.Threading;
using Windows.UI.Xaml;

//
// The namespace for the background tasks.
//
namespace Trogon.SportOn.TracingTask
{
    //
    // A background task always implements the IBackgroundTask interface.
    //
    public sealed class SampleBackgroundTask : IBackgroundTask
    {
        BackgroundTaskCancellationReason _cancelReason = BackgroundTaskCancellationReason.Abort;
        volatile bool _cancelRequested = false;
        BackgroundTaskDeferral _deferral = null;
        IBackgroundTaskInstance _taskInstance = null;

        bool _isTracking;
        Geolocator _geolocator = null;
        Geoposition _lastPosition;
        DateTime _startTime;
        double _totalDistance;
        Random rand = null;

        //
        // The Run method is the entry point of a background task.
        //
        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            Debug.WriteLine("Background " + taskInstance.Task.Name + " Starting...");

            //
            // Query BackgroundWorkCost
            // Guidance: If BackgroundWorkCost is high, then perform only the minimum amount
            // of work in the background task and return immediately.
            //
            var cost = BackgroundWorkCost.CurrentBackgroundWorkCost;
            var settings = ApplicationData.Current.LocalSettings;
            settings.Values["BackgroundWorkCost"] = cost.ToString();

            _isTracking = false;
            _startTime = new DateTime((long)settings.Values["StartTime"]);
            _totalDistance = 0;
            rand = new Random((int)_startTime.Ticks);

            //
            // Associate a cancellation handler with the background task.
            //
            taskInstance.Canceled += new BackgroundTaskCanceledEventHandler(OnCanceled);

            //
            // Get the deferral object from the task instance, and take a reference to the taskInstance;
            //
            _deferral = taskInstance.GetDeferral();
            _taskInstance = taskInstance;

            // Request permission to access location
            var accessStatus = await Geolocator.RequestAccessAsync();

            switch (accessStatus)
            {
                case GeolocationAccessStatus.Allowed:
                    // You should set MovementThreshold for distance-based tracking
                    // or ReportInterval for periodic-based tracking before adding event
                    // handlers. If none is set, a ReportInterval of 1 second is used
                    // as a default and a position will be returned every 1 second.
                    //
                    // Value of 2000 milliseconds (2 seconds) 
                    // isn't a requirement, it is just an example.
                    _geolocator = new Geolocator { ReportInterval = 2000 };

                    // Subscribe to PositionChanged event to get updated tracking positions
                    _geolocator.PositionChanged += OnPositionChanged;

                    // Subscribe to StatusChanged event to get updates of location status changes
                    _geolocator.StatusChanged += OnStatusChanged;

                    //_rootPage.NotifyUser("Waiting for update...", NotifyType.StatusMessage);
                    settings.Values["LocationDisabledMessageVisibility"] = (int)Visibility.Collapsed;
                    _isTracking = true;
                    break;

                case GeolocationAccessStatus.Denied:
                    //_rootPage.NotifyUser("Access to location is denied.", NotifyType.ErrorMessage);
                    settings.Values["LocationDisabledMessageVisibility"] = (int)Visibility.Visible;
                    break;

                case GeolocationAccessStatus.Unspecified:
                    //_rootPage.NotifyUser("Unspecificed error!", NotifyType.ErrorMessage);
                    settings.Values["LocationDisabledMessageVisibility"] = (int)Visibility.Collapsed;
                    break;
            }

            _taskInstance.Progress++;
        }

        private void OnPositionChanged(Geolocator sender, PositionChangedEventArgs e)
        {
            UpdateLocationData(e.Position);
            _taskInstance.Progress++;

            CleanUpCheck(false);
        }

        private void OnStatusChanged(Geolocator sender, StatusChangedEventArgs e)
        {
            var settings = ApplicationData.Current.LocalSettings;
            // Show the location setting message only if status is disabled.
            settings.Values["LocationDisabledMessageVisibility"] = (int)Visibility.Collapsed;

            switch (e.Status)
            {
                case PositionStatus.Ready:
                    // Location platform is providing valid data.
                    settings.Values["Status"] = "Ready";
                    //_rootPage.NotifyUser("Location platform is ready.", NotifyType.StatusMessage);
                    break;

                case PositionStatus.Initializing:
                    // Location platform is attempting to acquire a fix. 
                    settings.Values["Status"] = "Initializing";
                    //_rootPage.NotifyUser("Location platform is attempting to obtain a position.", NotifyType.StatusMessage);
                    break;

                case PositionStatus.NoData:
                    // Location platform could not obtain location data.
                    settings.Values["Status"] = "No data";
                    //_rootPage.NotifyUser("Not able to determine the location.", NotifyType.ErrorMessage);
                    break;

                case PositionStatus.Disabled:
                    // The permission to access location data is denied by the user or other policies.
                    settings.Values["Status"] = "Disabled";
                    //_rootPage.NotifyUser("Access to location is denied.", NotifyType.ErrorMessage);

                    // Show message to the user to go to location settings
                    settings.Values["LocationDisabledMessageVisibility"] = (int)Visibility.Visible;

                    // Clear cached location data if any
                    UpdateLocationData(null);
                    break;

                case PositionStatus.NotInitialized:
                    // The location platform is not initialized. This indicates that the application 
                    // has not made a request for location data.
                    settings.Values["Status"] = "Not initialized";
                    //_rootPage.NotifyUser("No request for location is made yet.", NotifyType.StatusMessage);
                    break;

                case PositionStatus.NotAvailable:
                    // The location platform is not available on this version of the OS.
                    settings.Values["Status"] = "Not available";
                    //_rootPage.NotifyUser("Location is not available on this version of the OS.", NotifyType.ErrorMessage);
                    break;

                default:
                    settings.Values["Status"] = "Unknown";
                    //_rootPage.NotifyUser(string.Empty, NotifyType.StatusMessage);
                    break;
            }

            _taskInstance.Progress++;

            CleanUpCheck(false);
        }

        //
        // Handles background task cancellation.
        //
        private void OnCanceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            //
            // Indicate that the background task is canceled.
            //
            _cancelRequested = true;
            _cancelReason = reason;

            CleanUpCheck(true);

            Debug.WriteLine("Background " + sender.Task.Name + " Cancel Requested...");
        }

        private void CleanUpCheck(bool isCanceled)
        {
            if ((_cancelRequested == true) || (_isTracking == false) || (isCanceled == true))
            {
                _geolocator.PositionChanged -= OnPositionChanged;
                _geolocator.StatusChanged -= OnStatusChanged;
                _geolocator = null;

                var key = _taskInstance.Task.Name;

                //
                // Record that this background task ran.
                //
                String taskStatus = "Canceled with reason: " + _cancelReason.ToString();
                var settings = ApplicationData.Current.LocalSettings;
                settings.Values[key] = taskStatus;

                Debug.WriteLine("Background " + _taskInstance.Task.Name + settings.Values[key]);

                _isTracking = false;

                //
                // Indicate that the background task has completed.
                //
                _deferral.Complete();
            }
        }

        private void UpdateLocationData(Geoposition position)
        {
            var settings = ApplicationData.Current.LocalSettings;

            if (position == null)
            {
                settings.Values["Latitude"] = null;
                settings.Values["Longitude"] = null;
                settings.Values["Distance"] = null;
                settings.Values["Accuracy"] = null;

                CleanUpCheck(true);
            }
            else
            {
                settings.Values["Latitude"] = position.Coordinate.Point.Position.Latitude;
                settings.Values["Longitude"] = position.Coordinate.Point.Position.Longitude;
                settings.Values["Accuracy"] = position.Coordinate.Accuracy;

                if (_lastPosition != null)
                {
                    _totalDistance += GetDistanceTo(_lastPosition.Coordinate, position.Coordinate);
                    settings.Values["Distance"] = _totalDistance;
                }

                TimeSpan time = DateTime.Now - _startTime;
                double speedKmH = _totalDistance / 1000.0 / time.TotalHours;

                _lastPosition = position;
                if (_startTime == DateTime.MinValue)
                {
                    _startTime = position.Coordinate.Timestamp.LocalDateTime;
                }
            }
        }

        public static double GetDistanceTo(Geocoordinate @this, Geocoordinate other)
        {
            if (double.IsNaN(@this.Point.Position.Latitude) || double.IsNaN(@this.Point.Position.Longitude) || double.IsNaN(other.Point.Position.Latitude) || double.IsNaN(other.Point.Position.Longitude))
            {
                throw new ArgumentException("Argument_LatitudeOrLongitudeIsNotANumber");
            }
            else
            {
                double latitude = @this.Point.Position.Latitude * 0.0174532925199433;
                double longitude = @this.Point.Position.Longitude * 0.0174532925199433;
                double num = other.Point.Position.Latitude * 0.0174532925199433;
                double longitude1 = other.Point.Position.Longitude * 0.0174532925199433;
                double num1 = longitude1 - longitude;
                double num2 = num - latitude;
                double num3 = Math.Pow(Math.Sin(num2 / 2), 2) + Math.Cos(latitude) * Math.Cos(num) * Math.Pow(Math.Sin(num1 / 2), 2);
                double num4 = 2 * Math.Atan2(Math.Sqrt(num3), Math.Sqrt(1 - num3));
                double num5 = 6376500 * num4;
                return num5;
            }
        }
    }
}
