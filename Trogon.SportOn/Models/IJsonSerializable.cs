﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;

namespace Trogon.SportOn.Models
{
    interface IJsonSerializable
    {
        void InitFromJsonString(string jsonString);

        void InitFromJsonObject(JsonObject jsonObject);

        JsonObject ToJsonObject();

        string Stringify();
    }
}
