using Newtonsoft.Json.Linq;
using System;
using Windows.Data.Json;

namespace Trogon.SportOn.Models
{
    public class Workout : IJsonSerializable
    {
        private const string idKey = "id";
        private const string sportKey = "sport";
        private const string startTimeKey = "startTime";
        private const string durationKey = "duration";
        private const string distanceKey = "distance";

        public string WorkoutId { get; set; }
        public Sport Sport { get; set; }
        public DateTime StartTime { get; set; }
        public TimeSpan? Duration { get; set; }
        public double Distance { get; set; }

        public Workout()
        { 
        }

        public Workout(string jsonString)
        {
            InitFromJsonString(jsonString);
        }

        public Workout(JsonObject jsonObject)
        {
            InitFromJsonObject(jsonObject);
        }

        public void InitFromJsonString(string jsonString)
        {
            JsonObject jsonObject = JsonObject.Parse(jsonString);
            InitFromJsonObject(jsonObject);
        }

        public void InitFromJsonObject(JsonObject jsonObject)
        {
            WorkoutId = jsonObject.GetNamedString(idKey);
            Sport = (Sport)(int)jsonObject.GetNamedNumber(sportKey);
            StartTime = new DateTime((long)jsonObject.GetNamedNumber(startTimeKey));
            IJsonValue phoneJsonValue = jsonObject.GetNamedValue(durationKey);
            if (phoneJsonValue.ValueType == JsonValueType.Null)
                Duration = null;
            else
                Duration = new TimeSpan((long)phoneJsonValue.GetNumber());
            Distance = jsonObject.GetNamedNumber(distanceKey);

            //foreach (IJsonValue jsonValue in jsonObject.GetNamedArray(educationKey, new JsonArray()))
            //{
            //    if (jsonValue.ValueType == JsonValueType.Object)
            //    {
            //        Education.Add(new School(jsonValue.GetObject()));
            //    }
            //}
        }

        public JsonObject ToJsonObject()
        {
            JsonObject jsonObject = new JsonObject();
            jsonObject.SetNamedValue(idKey, JsonValue.CreateStringValue(WorkoutId));
            jsonObject.SetNamedValue(sportKey, JsonValue.CreateNumberValue((int)Sport));
            jsonObject.SetNamedValue(startTimeKey, JsonValue.CreateNumberValue(StartTime.Ticks));
            if (Duration.HasValue)
                jsonObject.SetNamedValue(durationKey, JsonValue.CreateNumberValue(Duration.Value.Ticks));
            else
                jsonObject.SetNamedValue(durationKey, JsonValue.CreateNullValue());
            jsonObject.SetNamedValue(distanceKey, JsonValue.CreateNumberValue(Distance));

            //JsonArray jsonArray = new JsonArray();
            //foreach (School school in Education)
            //{
            //    jsonArray.Add(school.ToJsonObject());
            //}
            //jsonObject.SetNamedValue(educationKey, jsonArray);

            return jsonObject;
        }

        public string Stringify()
        {
            JsonObject jsonObject = ToJsonObject();

            return jsonObject.Stringify();
        }
    }
}
