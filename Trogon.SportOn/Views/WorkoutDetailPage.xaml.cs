using Trogon.SportOn.Models;
using Trogon.SportOn.ViewModels;

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Trogon.SportOn.Views
{
    public sealed partial class WorkoutDetailPage : Page
    {
        public WorkoutDetailViewModel ViewModel { get; } = new WorkoutDetailViewModel();
        public WorkoutDetailPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            ViewModel.Item = e.Parameter as Order;
        }
    }
}
