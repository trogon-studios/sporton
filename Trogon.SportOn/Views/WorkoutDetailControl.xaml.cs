using Trogon.SportOn.Models;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Trogon.SportOn.Views
{
    public sealed partial class WorkoutDetailControl : UserControl
    {
        public Order MasterMenuItem
        {
            get { return GetValue(MasterMenuItemProperty) as Order; }
            set { SetValue(MasterMenuItemProperty, value); }
        }

        public static DependencyProperty MasterMenuItemProperty = DependencyProperty.Register("MasterMenuItem",typeof(Order),typeof(WorkoutDetailControl),new PropertyMetadata(null));

        public WorkoutDetailControl()
        {
            InitializeComponent();
        }
    }
}
