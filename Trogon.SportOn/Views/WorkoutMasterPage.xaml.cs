using Trogon.SportOn.ViewModels;

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Trogon.SportOn.Views
{
    public sealed partial class WorkoutMasterPage : Page
    {
        public WorkoutMasterViewModel ViewModel { get; } = new WorkoutMasterViewModel();
        public WorkoutMasterPage()
        {
            InitializeComponent();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            await ViewModel.LoadDataAsync(WindowStates.CurrentState);
        }
    }
}
