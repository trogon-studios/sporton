using Trogon.SportOn.Models;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Trogon.SportOn.Views
{
    public sealed partial class MasterDetailDetailControl : UserControl
    {
        public Workout MasterMenuItem
        {
            get { return GetValue(MasterMenuItemProperty) as Workout; }
            set { SetValue(MasterMenuItemProperty, value); }
        }

        public static DependencyProperty MasterMenuItemProperty = DependencyProperty.Register("MasterMenuItem",typeof(Workout),typeof(MasterDetailDetailControl),new PropertyMetadata(null));

        public MasterDetailDetailControl()
        {
            InitializeComponent();
        }
    }
}
