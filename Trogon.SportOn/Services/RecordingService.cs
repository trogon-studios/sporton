﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Storage;

namespace Trogon.SportOn.Services
{
    internal class RecordingService
    {
        private const string trackFileName = "track.json";

        public List<BasicGeoposition> TrackPoints { get; } = new List<BasicGeoposition>();

        public async Task InitStorage()
        {
            TrackPoints.Clear();

            await SaveStorage();
        }

        public async Task LoadStorage()
        {
            TrackPoints.Clear();

            var trackFile = await ApplicationData.Current.TemporaryFolder.TryGetItemAsync(trackFileName);
            if (trackFile != null)
            {
                var trackText = File.ReadAllText(trackFile.Path);
                TrackPoints.AddRange(JsonConvert.DeserializeObject<List<BasicGeoposition>>(trackText));
            }
        }

        public async Task SaveStorage()
        {
            var trackFile = await ApplicationData.Current.TemporaryFolder.TryGetItemAsync(trackFileName);
            if (trackFile == null)
            {
                trackFile = await ApplicationData.Current.TemporaryFolder.CreateFileAsync(trackFileName);
            }
            var trackText = JsonConvert.SerializeObject(TrackPoints);
            File.WriteAllText(trackFile.Path, trackText);
        }

        public async Task ClearStorage()
        {
            TrackPoints.Clear();

            var trackFile = await ApplicationData.Current.TemporaryFolder.TryGetItemAsync(trackFileName);
            if (trackFile != null)
            {
                File.Delete(trackFile.Path);
            }
        }
    }
}
