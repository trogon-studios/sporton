using System;

namespace Trogon.SportOn.Services
{
    public class SuspensionState
    {
        public Object Data { get; set; }
        public DateTime SuspensionDate { get; set; }
    }
}
