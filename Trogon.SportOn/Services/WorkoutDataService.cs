using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Threading.Tasks;

using Windows.Data.Json;
using Windows.Storage;

using Trogon.SportOn.Models;

namespace Trogon.SportOn.Services
{
    public static class WorkoutDataService
    {
        private const string workoutsFolderName = "Workouts";
        private static List<Workout> workouts;

        private async static Task LoadData()
        {
            StorageFolder workoutsFolder;
            try
            {
                workoutsFolder = await ApplicationData.Current.RoamingFolder.GetFolderAsync(workoutsFolderName);
            }
            catch (FileNotFoundException)
            {
                workoutsFolder = await ApplicationData.Current.RoamingFolder.CreateFolderAsync(workoutsFolderName);
            }

            var workoutFiles = await workoutsFolder.GetFilesAsync();

            workouts = new List<Workout>();
            foreach (var workoutFile in workoutFiles)
            {
                string content = await FileIO.ReadTextAsync(workoutFile);
                workouts.Add(ParseWorkout(content));
            }
        }

        public async static Task SaveWorkout(Workout workout)
        {
            if (string.IsNullOrEmpty(workout.WorkoutId))
            {
                workout.WorkoutId = $"Workout_{workout.StartTime.Ticks}_{workout.Sport}";
            }

            StorageFolder workoutsFolder;
            try
            {
                workoutsFolder = await ApplicationData.Current.RoamingFolder.GetFolderAsync(workoutsFolderName);
            }
            catch (FileNotFoundException)
            {
                workoutsFolder = await ApplicationData.Current.RoamingFolder.CreateFolderAsync(workoutsFolderName);
            }

            string workoutFileName = $"{workout.WorkoutId}.json";
            StorageFile workoutFile;
            try
            {
                workoutFile = await workoutsFolder.GetFileAsync(workoutFileName);
            }
            catch (FileNotFoundException)
            {
                workoutFile = await workoutsFolder.CreateFileAsync(workoutFileName);
            }

            string content = workout.Stringify();
            await FileIO.WriteTextAsync(workoutFile, content);
            string some = await FileIO.ReadTextAsync(workoutFile);
        }

        public async static Task DeleteWorkout(Workout workout)
        {
            try
            {
                StorageFolder workoutsFolder = await ApplicationData.Current.RoamingFolder.GetFolderAsync(workoutsFolderName);
                string workoutFileName = $"{workout.WorkoutId}.json";
                StorageFile workoutFile = await workoutsFolder.GetFileAsync(workoutFileName);
                await workoutFile.DeleteAsync();
            }
            catch (FileNotFoundException) { }
        }

        private async static Task PrepareInitial()
        {
            var rand = new Random((int)DateTime.Now.Ticks);
            for (int i = 0; i < 20; i++)
            {
                DateTime startTime = DateTime.Now;
                Sport sport = (Sport)(new Random()).Next(10, 40);
                workouts.Add(new Workout()
                {
                    WorkoutId = $"Workout_{startTime.Ticks}_{sport}",
                    Sport = sport,
                    StartTime = startTime,
                    Distance = rand.Next(10, 20),
                    Duration = new TimeSpan(rand.Next(10, 20) * 20000),
                });
                await Task.Delay(10);
            }
        }

        private static IEnumerable<Workout> AllWorkouts()
        {
            return workouts;
        }

        private static IEnumerable<Workout> WorkoutsByName(string name)
        {
            return workouts.Where(x => x.WorkoutId == name);
        }

        public static async Task<IEnumerable<Workout>> GetWorkoutsAsync()
        {
            await LoadData();

            return AllWorkouts();
        }

        private static Workout ParseWorkout(string content)
        {
            try
            {
                return new Workout(content);
                //rootPage.NotifyUser("JSON string parsed successfully.", NotifyType.StatusMessage);
            }
            catch (Exception ex)
            {
                if (!IsExceptionHandled(ex))
                {
                    throw ex;
                }
            }
            return null;
        }

        private static string StringifyWorkout(Workout entity)
        {
            Debug.Assert(entity != null);

            return entity.Stringify();
        }

        private static bool IsExceptionHandled(Exception ex)
        {
            JsonErrorStatus error = JsonError.GetJsonStatus(ex.HResult);
            if (error == JsonErrorStatus.Unknown)
            {
                return false;
            }

            //rootPage.NotifyUser(error + ": " + ex.Message, NotifyType.ErrorMessage);
            return true;
        }
    }
}
