using System;

using Trogon.SportOn.Helpers;
using Trogon.SportOn.Services;

using Windows.ApplicationModel.Background;
using Windows.Devices.Geolocation;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace Trogon.SportOn.ViewModels
{
    public class MainViewModel : Observable
    {
        public MainViewModel()
        {
            LocationDisabledMessageVisibility = Visibility.Collapsed;

            StartTrackingCommand = new RelayCommand(StartTrackingAction, CanExecuteStartTracking);
            StopTrackingCommand = new RelayCommand(StopTrackingAction, CanExecuteStopTracking);
        }

        // Proides access to location data
        private CoreDispatcher _dispatcher;
        private bool _isTracking;
        // A pointer to the ApplicationTrigger so we can signal it later
        ApplicationTrigger trigger = null;

        public RelayCommand StartTrackingCommand { get; }
        public RelayCommand StopTrackingCommand { get; }

        private string _status;
        public string Status
        {
            get { return _status; }
            set { Set(ref _status, value); }
        }

        private Visibility _locationDisabledMessageVisibility;
        public Visibility LocationDisabledMessageVisibility
        {
            get { return _locationDisabledMessageVisibility; }
            set { Set(ref _locationDisabledMessageVisibility, value); }
        }

        private double? _latitude;
        public double? Latitude
        {
            get { return _latitude; }
            set { Set(ref _latitude, value); }
        }

        private double? _longitude;
        public double? Longitude
        {
            get { return _longitude; }
            set { Set(ref _longitude, value); }
        }

        private double? _distance;
        public double? Distance
        {
            get { return _distance; }
            set { Set(ref _distance, value); }
        }

        private double? _accuracy;
        public double? Accuracy
        {
            get { return _accuracy; }
            set { Set(ref _accuracy, value); }
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached. The Parameter
        /// property is typically used to configure the page.</param>
        internal void Init(CoreDispatcher dispatcher)
        {
            _dispatcher = dispatcher;

            foreach (var task in BackgroundTaskRegistration.AllTasks)
            {
                if (task.Value.Name == BackgroundTaskService.ApplicationTriggerTaskName)
                {
                    AttachProgressAndCompletedHandlers(task.Value);
                    BackgroundTaskService.UpdateBackgroundTaskRegistrationStatus(BackgroundTaskService.ApplicationTriggerTaskName, true);
                    break;
                }
            }

            trigger = new ApplicationTrigger();
        }

        /// <summary>
        /// Invoked immediately before the Page is unloaded and is no longer the current source of a parent Frame.
        /// </summary>
        /// <param name="e">
        /// Event data that can be examined by overriding code. The event data is representative
        /// of the navigation that will unload the current Page unless canceled. The
        /// navigation can potentially be canceled by setting e.Cancel to true.
        /// </param>
        internal void Clean()
        {

        }

        private bool CanExecuteStartTracking()
        {
            return !_isTracking;
        }

        /// <summary>
        /// This is the click handler for the 'StartTracking' button.
        /// </summary>
        private async void StartTrackingAction()
        {
            // Reset the completion status
            var settings = ApplicationData.Current.LocalSettings;
            settings.Values["Status"] = string.Empty;
            settings.Values["LocationDisabledMessageVisibility"] = (int)Visibility.Collapsed;
            settings.Values["Latitude"] = null;
            settings.Values["Longitude"] = null;
            settings.Values["Distance"] = null;
            settings.Values["Accuracy"] = null;
            settings.Values["StartTime"] = DateTime.Now.Ticks;

            settings.Values.Remove(BackgroundTaskService.ApplicationTriggerTaskName);

            // Request permission to access location
            var accessStatus = await Geolocator.RequestAccessAsync();

            switch (accessStatus)
            {
                case GeolocationAccessStatus.Allowed:
                    //_rootPage.NotifyUser("Waiting for update...", NotifyType.StatusMessage);
                    settings.Values["LocationDisabledMessageVisibility"] = (int)Visibility.Collapsed;
                    _isTracking = true;
                    break;

                case GeolocationAccessStatus.Denied:
                    //_rootPage.NotifyUser("Access to location is denied.", NotifyType.ErrorMessage);
                    settings.Values["LocationDisabledMessageVisibility"] = (int)Visibility.Visible;
                    break;

                case GeolocationAccessStatus.Unspecified:
                    //_rootPage.NotifyUser("Unspecificed error!", NotifyType.ErrorMessage);
                    settings.Values["LocationDisabledMessageVisibility"] = (int)Visibility.Collapsed;
                    break;
            }

            var task = BackgroundTaskService.RegisterBackgroundTask(BackgroundTaskService.SampleBackgroundTaskEntryPoint,
                                                           BackgroundTaskService.ApplicationTriggerTaskName,
                                                           trigger,
                                                           null);
            AttachProgressAndCompletedHandlers(task);
            UpdateUI();

            //Signal the ApplicationTrigger
            var result = await trigger.RequestAsync();
            BackgroundTaskService.ApplicationTriggerTaskResult = "Signal result: " + result.ToString();
            UpdateUI();
        }

        private bool CanExecuteStopTracking()
        {
            return _isTracking;
        }

        /// <summary>
        /// This is the click handler for the 'StopTracking' button.
        /// </summary>
        private void StopTrackingAction()
        {
            BackgroundTaskService.UnregisterBackgroundTasks(BackgroundTaskService.ApplicationTriggerTaskName);
            BackgroundTaskService.ApplicationTriggerTaskResult = "";

            trigger = new ApplicationTrigger();

            UpdateUI();
        }

        /// <summary>
        /// Attach progress and completed handers to a background task.
        /// </summary>
        /// <param name="task">The task to attach progress and completed handlers to.</param>
        private void AttachProgressAndCompletedHandlers(IBackgroundTaskRegistration task)
        {
            _isTracking = true;

            task.Progress += new BackgroundTaskProgressEventHandler(OnProgress);
            task.Completed += new BackgroundTaskCompletedEventHandler(OnCompleted);
        }

        /// <summary>
        /// Handle background task progress.
        /// </summary>
        /// <param name="task">The task that is reporting progress.</param>
        /// <param name="e">Arguments of the progress report.</param>
        private void OnProgress(IBackgroundTaskRegistration task, BackgroundTaskProgressEventArgs args)
        {
            var ignored = _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                var progress = "Progress: " + args.Progress + "%";
                BackgroundTaskService.ApplicationTriggerTaskProgress = progress;
                UpdateUI();
            });
        }

        /// <summary>
        /// Handle background task completion.
        /// </summary>
        /// <param name="task">The task that is reporting completion.</param>
        /// <param name="e">Arguments of the completion report.</param>
        private void OnCompleted(IBackgroundTaskRegistration task, BackgroundTaskCompletedEventArgs args)
        {
            _isTracking = false;

            var settings = ApplicationData.Current.LocalSettings;
            var _startTime = new DateTime((long)settings.Values["StartTime"]);

            var workout = new Models.Workout()
            {
                StartTime = _startTime,
                Sport = Models.Sport.Other,
                Distance = Distance.Value,
                Duration = DateTime.Now - _startTime,
            };
            WorkoutDataService.SaveWorkout(workout);

            settings.Values["Status"] = string.Empty;
            settings.Values["LocationDisabledMessageVisibility"] = (int)Visibility.Collapsed;
            settings.Values["Latitude"] = null;
            settings.Values["Longitude"] = null;
            settings.Values["Distance"] = null;
            settings.Values["Accuracy"] = null;
            settings.Values["StartTime"] = DateTime.Now.Ticks;

            UpdateUI();
        }

        /// <summary>
        /// Update the scenario UI.
        /// </summary>
        private async void UpdateUI()
        {
            await _dispatcher.RunAsync(CoreDispatcherPriority.Normal,
            () =>
            {
                var settings = ApplicationData.Current.LocalSettings;
                Status = (string)settings.Values["Status"];
                LocationDisabledMessageVisibility = (Visibility)settings.Values["LocationDisabledMessageVisibility"];
                Latitude = (double?)settings.Values["Latitude"];
                Longitude = (double?)settings.Values["Longitude"];
                Distance = (double?)settings.Values["Distance"];
                Accuracy = (double?)settings.Values["Accuracy"];

                //RegisterButton.IsEnabled = !BackgroundTaskSample.ApplicationTriggerTaskRegistered;
                //UnregisterButton.IsEnabled = BackgroundTaskSample.ApplicationTriggerTaskRegistered;
                //SignalButton.IsEnabled = BackgroundTaskSample.ApplicationTriggerTaskRegistered & (trigger != null);
                //Progress.Text = BackgroundTaskSample.ApplicationTriggerTaskProgress;
                //Result.Text = BackgroundTaskSample.ApplicationTriggerTaskResult;
                //Status.Text = BackgroundTaskSample.GetBackgroundTaskStatus(BackgroundTaskSample.ApplicationTriggerTaskName);

                StartTrackingCommand.OnCanExecuteChanged();
                StopTrackingCommand.OnCanExecuteChanged();
            });
        }
    }
}
