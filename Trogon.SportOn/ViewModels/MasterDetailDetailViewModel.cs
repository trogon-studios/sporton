using System;
using System.Windows.Input;

using Trogon.SportOn.Helpers;
using Trogon.SportOn.Models;
using Trogon.SportOn.Services;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace Trogon.SportOn.ViewModels
{
    public class MasterDetailDetailViewModel : Observable
    {
        const string NarrowStateName = "NarrowState";
        const string WideStateName = "WideState";

        public ICommand StateChangedCommand { get; private set; }
        public RelayCommand DeleteCommand { get; }

        private Workout _item;
        private CoreDispatcher _dispatcher;

        public Workout Item
        {
            get { return _item; }
            set { Set(ref _item, value); }
        }

        public MasterDetailDetailViewModel()
        {
            StateChangedCommand = new RelayCommand<VisualStateChangedEventArgs>(OnStateChanged);
            DeleteCommand = new RelayCommand(DeleteAction, CanExecuteDelete);
        }

        private void DeleteAction()
        {
            WorkoutDataService.DeleteWorkout(Item).ContinueWith(
                (antecedent) => _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        NavigationService.GoBack();
                    })
            );
        }

        private bool CanExecuteDelete()
        {
            return true;
        }

        private void OnStateChanged(VisualStateChangedEventArgs args)
        {
            if (args.OldState.Name == NarrowStateName && args.NewState.Name == WideStateName)
            {
                NavigationService.GoBack();
            }
        }

        internal void Init(CoreDispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }

        internal void Clean()
        {
            _dispatcher = null;
        }
    }
}
